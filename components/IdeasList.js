"use client";
import { useState, useEffect } from 'react';
import axios from 'axios';
import { useInView } from 'react-intersection-observer';
import Image from 'next/image';

const IdeasList = () => {
  const [ideas, setIdeas] = useState([]);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [sort, setSort] = useState('-published_at');
  const [totalIdeas, setTotalIdeas] = useState(0);
  const [currentPageSet, setCurrentPageSet] = useState(1);
  const [isMounted, setIsMounted] = useState(false);
  const { ref, inView } = useInView({
    threshold: 0.1,
  });

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const savedPage = localStorage.getItem('currentPage');
      const savedPageSize = localStorage.getItem('pageSize');
      const savedSort = localStorage.getItem('sort');

      if (savedPage) setPage(Number(savedPage));
      if (savedPageSize) setPageSize(Number(savedPageSize));
      if (savedSort) setSort(savedSort);

      setIsMounted(true);
    }
  }, []);

  useEffect(() => {
    const fetchIdeas = async () => {
      const response = await axios.get('/api/ideas', {
        params: {
          'page[number]': page,
          'page[size]': pageSize,
          append: ['small_image', 'medium_image'],
          sort: sort,
        },
      });
      setIdeas(response.data.data);
      setTotalIdeas(response.data.meta.total); // Assuming your API response includes a meta field with total count
    };

    fetchIdeas();
  }, [page, pageSize, sort]);

  useEffect(() => {
    if (isMounted) {
      localStorage.setItem('currentPage', page);
    }
  }, [page, isMounted]);

  useEffect(() => {
    if (isMounted) {
      localStorage.setItem('pageSize', pageSize);
      localStorage.setItem('currentPage', 1); // Reset to page 1 when pageSize changes
    }
  }, [pageSize, isMounted]);

  useEffect(() => {
    if (isMounted) {
      localStorage.setItem('sort', sort);
      localStorage.setItem('currentPage', 1); // Reset to page 1 when sort changes
    }
  }, [sort, isMounted]);

  const handlePageChange = (newPage) => {
    setPage(newPage);
  };

  const handlePageSizeChange = (newPageSize) => {
    setPageSize(newPageSize);
    setPage(1); // Reset to the first page whenever the page size changes
  };

  const handleSortChange = (newSort) => {
    setSort(newSort);
    setPage(1); // Reset to the first page whenever the sort order changes
  };

  const totalPages = Math.ceil(totalIdeas / pageSize);
  const maxPageButtons = 5;
  const currentPageSetStart = (currentPageSet - 1) * maxPageButtons + 1;

  const incrementPageSet = () => {
    if (currentPageSetStart + maxPageButtons <= totalPages) {
      setCurrentPageSet(currentPageSet + 1);
    }
  };

  const decrementPageSet = () => {
    if (currentPageSet > 1) {
      setCurrentPageSet(currentPageSet - 1);
    }
  };

  if (!isMounted) {
    return null; // Render nothing on the server side or during initial mount
  }

  return (
    <div className="container mx-auto p-5">
      <div className="flex justify-between mb-5">
        <div>Showing {((page - 1) * pageSize) + 1} - {Math.min(page * pageSize, totalIdeas)} of {totalIdeas}</div>
        <div className="flex space-x-4">
          <div>
            <label className="mr-2">Show per page:</label>
            <select onChange={(e) => handlePageSizeChange(Number(e.target.value))} className="border p-2" value={pageSize}>
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="50">50</option>
            </select>
          </div>
          <div>
            <label className="mr-2">Sort by:</label>
            <select onChange={(e) => handleSortChange(e.target.value)} className="border p-2" value={sort}>
              <option value="-published_at">Newest</option>
              <option value="published_at">Oldest</option>
            </select>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
        {ideas.map((idea) => {
          const imageUrl = idea.small_image || idea.medium_image;
          return (
            <div key={idea.id} className="border rounded p-4 shadow-sm">
              {imageUrl ? (
                <Image
                  src={imageUrl}
                  alt={idea.title}
                  width={500}
                  height={200}
                  className="w-full h-40 object-cover"
                />
              ) : (
                <div className="w-full h-40 bg-gray-200 flex items-center justify-center">
                  <span>No Image</span>
                </div>
              )}
              <h3 className="mt-4 text-lg font-semibold line-clamp-3">{idea.title}</h3>
            </div>
          );
        })}
      </div>
      <div className="flex justify-center mt-4 space-x-2">
        <button className="px-4 py-2 bg-gray-200" onClick={decrementPageSet} disabled={currentPageSet === 1}>
          &lt;
        </button>
        {Array.from({ length: Math.min(maxPageButtons, totalPages - currentPageSetStart + 1) }, (_, i) => (
          <button
            key={i}
            className={`px-4 py-2 ${page === currentPageSetStart + i ? 'bg-gray-400' : 'bg-gray-200'}`}
            onClick={() => handlePageChange(currentPageSetStart + i)}
          >
            {currentPageSetStart + i}
          </button>
        ))}
        <button className="px-4 py-2 bg-gray-200" onClick={incrementPageSet} disabled={currentPageSetStart + maxPageButtons > totalPages}>
          &gt;
        </button>
      </div>
    </div>
  );
};

export default IdeasList;

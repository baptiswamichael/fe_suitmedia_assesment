// components/Header.js
"use client";
import { useState, useEffect } from 'react';
import Image from 'next/image';

const Header = () => {
  const [show, setShow] = useState(true);
  const [scrollPos, setScrollPos] = useState(0);

  useEffect(() => {
    const handleScroll = () => {
      setShow(window.scrollY < scrollPos || window.scrollY === 0);
      setScrollPos(window.scrollY);
    };
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [scrollPos]);

  return (
    <header className={`fixed w-full transition-all ${show ? 'top-0 bg-opacity-75' : '-top-20'} bg-orange-500 z-50`}>
      <div className="container mx-auto p-4">
        <nav className="flex justify-between items-center">
          <div className="flex-shrink-0">
            <Image src="/logo_sm.png" alt="Logo" width={50} height={50} className="w-auto h-auto" />
          </div>
          <ul className="flex space-x-4 text-white">
            <li>Work</li>
            <li>About</li>
            <li>Services</li>
            <li className="font-bold">Ideas</li>
            <li>Careers</li>
            <li>Contact</li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;

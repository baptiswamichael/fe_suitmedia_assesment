// components/Banner.js
"use client";
import { useEffect, useRef, useState } from 'react';
import axios from 'axios';

const Banner = () => {
  const [banner, setBanner] = useState(null);
  const parallaxRef = useRef();

  useEffect(() => {
    const fetchBanner = async () => {
      try {
        const response = await axios.get('https://methodical-presence-2883ca6107.strapiapp.com/api/banners?populate=*');
        if (response.data && response.data.data && response.data.data.length > 0) {
          const bannerData = response.data.data[0].attributes;
          setBanner(bannerData);
        } else {
          console.error('No banner data found');
        }
      } catch (error) {
        console.error('Error fetching banner:', error);
      }
    };

    fetchBanner();
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      if (parallaxRef.current) {
        parallaxRef.current.style.transform = `translateY(${scrollPosition * 0.5}px)`;
      }
    };
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  if (!banner || !banner.image || !banner.image.data) {
    return null; 
  }

  const imageUrl = banner.image.data.attributes.url;

  console.log('Image URL:', imageUrl); // Log the image URL to check if it's correct

  return (
    <div className="relative h-96 overflow-hidden">
      <div
        ref={parallaxRef}
        className="absolute w-full h-full bg-cover bg-center"
        style={{ backgroundImage: `url(${imageUrl.startsWith('http') ? imageUrl : `https://methodical-presence-2883ca6107.strapiapp.com${imageUrl}`})` }}
      />
      <div className="absolute bottom-0 left-0 w-full h-10 bg-white transform rotate-[-3deg] origin-bottom"></div>
    </div>
  );
};

export default Banner;

// src/app/page.js
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import IdeasList from '../../components/IdeasList';

const HomePage = () => (
  <>
    <Header />
    <Banner imageUrl="/ideas.jpg" />
    <IdeasList />
  </>
);

export default HomePage;
